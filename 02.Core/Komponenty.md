#to_do  #to_review 
## Bazowe Komponenty

Każdą stronę internetową można podzielić na małe jednostki zwane komponentami.
Na przykład, strona internetowa może być złożona z paska bocznego i nagłówka.
Komponenty mogą również być dalej podzielone, przykładowo pasek boczny może składać się z kilku częśći.

> Komponent to element interfejsu użytkownika (interfejs użytkownika), który ma własną logikę i wygląd. Komponent może być tak mały jak przycisk lub duży jak cała strona. 
> [React docs](https://react.dev/learn#components)

Na początku React miał tylko [klasy komponentowe](https://pl.legacy.reactjs.org/docs/components-and-props.html).
Klasa komponentowa jest to klasa, która dziedziczy z klasy React.Component i wymaga implementacji metody render().

```js
class Welcome extends React.Component {
	return <h1>Hello, {this.props.name}</h1>
}
```

Jednak od 2018 roku, kiedy wprowadzono React 16.8, wykorzystywane są głównie komponenty funkcjonalne.
#MR: nie rozumiem tego zdania, prosiłbym o wytłumaczenie.  Czy chodzi o to, że przedstawia Pan ten sam komponent co powyżej, jako komponent funkcjonalny?
Ten sam prosty komponent, ale prowdzany do komponentu funkcjonalnego.
Główną ideą jest po prostu funkcja zwracająca JSX podobnie do funckcji render().

```js
function Welcome(props) {
  return <h1>Hello, {props.name}</h1>;
}
```

JSX to tylko małe rozszerzenie składni JavaScript, które upraszcza nasze życie poprzez pisanie znaczników przypominających HTML.
Aby przekazać js, zostanie on zawinięty w nawiasy klamrowe, jak poprzednio w prostych komponentach, co zostanie pokazane na następnym przykładzie.

W tej pracy będziemy używać tylko komponentów funkcjonalnych, ponieważ obecnie komponenty klasowe są rzadko używane nawet w projektach z silnymi typami, takimi jak w TypeScript.

## Zagnieżdżone komponenty i props

[Source of VaultsPage](https://github.com/Yaroslaw07/next_noti/blob/main/pages/vaults/index.tsx)
Wyświetlmy VaultsPage z mojej aplikacji Noti, aby pokazać bardziej złożone wykorzystanie komponentów.

```ts
const VaultsPage: NextPageWithLayout<VaultsPageProps> = ({
  vaults,
}: VaultsPageProps) => {
  ...
  return (
    <>
	  ..
      <NewVaultModal isOpen={isModalOpen} handleClose={handleModalClose} />
      <Container component="main" maxWidth="xs">
        <Stack ...>
          <Box ...>
            ...
          </Box>
          <Box ...>

            <VaultsList vaults={vaults} />
          </Box>
          <VaultsActions handleNewVault={handleModalOpen} />
        </Stack>
      </Container>
    </>
  );
};
```

Tutaj możemy faktycznie zobaczyć, że strona jest komponentem, który składa się z mniejszych komponentów, takich jak NewVaultModal, VaultList i VaultsAction.
Nazywa się to komponentami zagnieżdżonymi.

Ta strona ma parametry funkcji. 
W React nazwaliśmy je "props". 
Za ich pomocą możemy przekazać pewne argumenty do komponentu potomnego, tak jak w przypadku VaultsList przekazujemy "vaults". 
Może pojawić się pytanie, w jakis sposób prop jest przekazywany do strony.
Odpowiedzialne jest za to SSR (Server side rendering), za które odpowiada NextJs, a nie React. 
Dzięki niej na serwerze mogę wysłać żądanie pobrania wszystkich vaults, a mimo wszystko wysłać stronę Reacta już z tymi vaults. 


## Children prop #to_implenent

Są sytuacje, kiedy musimy opakować nasz komponent innym komponentem. Do tego zadania używamy rekwizytu children. Na przykładzie poniżej możemy zobaczyć, jak zrobić układ, który otacza nasze strony.

```tsx
const CurrentVaultLayout: FC<CurrentVaultLayoutProps> = ({ children }) => {
	...
	return (

    <Grid
      container ...
    >
      <Grid item>
        <Sidebar />
      </Grid>
      <Grid ... item>
        <Stack ...>
          <Header ... />
          {children}
        </Stack>
      </Grid>
    </Grid>
}
```
