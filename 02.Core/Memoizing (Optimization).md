React not ideal.
There are a lot of moments when React may unnecessarily update components.
We will discuss these situations, and a technique called memoization to prevent additional, unnecessary rerenders. 

Memozing in React tries to solve the problem of additional rerenders by caching the previous state of component.
This technique requires careful optimization of our app, since it is rather costly.
Using this techique causes the browser to consume more RAM and processor time.

React offers us a function memo() and two hooks: useMemo() and useCallback()
## memo()

For example we will take notesItem component. It is a component that represents one note in sidebar notes list.
Problem occurs when the orders of displaying notes is changed.
All notes will updated, because position of components will be changed in DOM.
But all recalculation in NotesItem and its children will be done one time more after the order is changed.

This is unnecessary.

To fix this issue we will use the memo() function:

```tsx
const NotesItem: FC<NotesItemProps> = ({ note }) => {
  ...

  return (
      <SidebarModule ...>
		...
      </SidebarModule>
  );
};

export default memo(NotesItem);
```

Now, before component is to be updated, React will check if props was changed from previous update. 
In this case all props are the same, so our component will not be updated. 

A second argument may also be passed to memo().
It is a function with custom logic of comparing props that needs return true if we need update of component, and false otherwise.
This is used in rare cases and our application does not need this additional complex logic.

# useMemo()

For memoizing some values inside component we will use useMemo().
It is ideal in situations when you have a complex logic of calculations or API calls.

Let's see an example in NotesItem component.

```tsx
const currentTitle: string = useMemo(
    () =>
      active
        ? !currentNoteTitle || currentNoteTitle === ""
          ? "Undefined"
          : currentNoteTitle.trim()
        : note.title.trim(),
    [note.title, currentNoteTitle, active]
  );
```

Here, the current title of the note is cached, so when note status of isPinned will change, the title not will be calculated.
The first parametr is a function that calculates value and after caching it, and the second parameter is list of dependencies like in useEffect().
The list of dependencies indicate when the cached value is not actual and needs to be updated.
It is not an ideal example of using useMemo(), as there are not too many recalculations to be performed.

## useCallback()

useCallback() is very similar to useMemo(), but instead of returning an value it returns a function.

```tsx
const handleOptionsClose = useCallback(
    (event: React.MouseEvent<HTMLElement>) => {
      event.stopPropagation();
      setIsHovered(false);
      setAnchorEl(null);
    },
    []
  );
```

So here we have a function inside NotesItem component, that closes additional options for the selected note. 
But because the list of dependencies is empty, it will be initialized only once on mount of component.
Notice, that this function will not be recreated every time React updates the component.
This is a good demonstration, as it should give us a boost of performance.
