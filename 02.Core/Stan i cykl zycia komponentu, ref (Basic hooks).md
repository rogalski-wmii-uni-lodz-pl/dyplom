#finished  #to_review 
## Hook

Aby kontynuować podstawy Reacta, musimy poznać nową koncepcję Reacta - hook

> Hooki są definiowane przy użyciu funkcji JavaScript, ale reprezentują specjalny typ logiki interfejsu użytkownika wielokrotnego użytku z ograniczeniami dotyczącymi tego, gdzie można je wywołać.
> - [React Team](https://react.dev/reference/rules/rules-of-hooks)

Wprowadzono je w React 16.8 z komponentem funkcjonalnym i zyskały dużą popularność w użytku osobistym i profesjonalnym.

Obowiązuje je kilka zasad:
- ich nazwa musi zaczynać się od przedrostka „use”
- nasze niestandardowe hooki muszą wykonać pewną jedną pracę, ale mogą być używane w rórznych miejscach aplikacji.
- hook należy wywoływac wyłącznie na najwyższym poziomie komponentu, nie wywołany automatyczne 
- hooki należy wywoływać wyłącznie z komponentu funkcjonalnego React

### Stan (useState)

W komponentach React możemy przechowywać pewne informacje, takie jak licznik, otwarcie modalne lub bieżący etap formularza. 
Informacje przechowywane w komponencie nazywamy stanem.
Stan component jest dynamiczny.
Zmiana stanu komponentu (np. wartości licznika) wywołuje ponownego wyrenderowania komponentu.

Teraz pokażemy użycie hooka - useState().

Możemy pokazać przypadek użycia useState na komponencie VaultsPage, który widzieliśmy wcześniej

```ts
const VaultsPage: NextPageWithLayout<VaultsPageProps> = ({

  vaults,

}: VaultsPageProps) => {
...
	const [isModalOpen, setIsModalOpen] = useState<boolean>(false);

	const handleModalOpen = () => setIsModalOpen(true);
	const handleModalClose = () => setIsModalOpen(false);

	return (

    <>
	  ...
      <NewVaultModal isOpen={isModalOpen} handleClose={handleModalClose} />
      ...
    </>
}
```

Hook useState zwraca dwie rzeczy, pierwsza to zmienna stanu, która jest faktycznie renderowana w naszym DOM, a druga to ustawiacz stanu, który wysyła naszą zmianę do DOM i uruchamia aplikację.

W naszym przykładzie ta wartość to po prostu flaga wskazująca, czy jest otwarty modal (w tym przypadku modal to formularz tworzenia nowego vaulta). 
Tę wartość flagi przekazujemy do naszego niestandardowego komponentu modalu jako jeden z propsów, ponieważ potrzebujemy jej do zmiany widoczności modalu.
Możemy także napisać funkcję taką jak hadleCloseModal i przekazać ją do komponentu. Daje nam to możliwość zamknięcia naszego komponentu w komponencie potomnym, gdy stan znajduje się w komponencie nadrzędnym.

### Cykl zycia i effecty(useEffect)

Komponenty React mają trzy stadia życia: montuj, odmontowuj lub aktualizuj. Mount, wtedy nasz komponent jest renderowany do DOM. Aktualizacja to proces ponownego renderowania komponentu lub jego części. Unmount to proces usuwania naszego komponentu z DOM.

Możemy dla naszego komponentu stworzyć efekt.
To działania poboczne komponentu, które można wykorzystać do wielu celów, takich jak pobieranie danych, animacja wyzwalania, subskrybowanie wydarzeń, wysyłanie statystyk i inne rzeczy.

Efekt może być wykonany kiedy się montuje i kiedy się odmontowuje.
Podczas aktualizacji po prostu wywołuje funkcję unmount i po zamontowaniu.
#MR: i po zamontowaniu?
Renderowanie komponentu nie aktualizuje efektu, ponieważ efekt może być aktualizowany tylko w przypadku niektórych stanów komponentów.

Efektem jest wykorzystanie wartości stanu tego, co zostało zamontowane. Jeśli stan się zmienił, ale nie w zależnościach, useEffect będzie miał starą wartość stanu.

Aby stworzyć efekt, musimy użyć haka useEffect(). Poniżej przyjrzymy się jego strukturze, ponieważ ona jest skomplikowana.

```ts
// wewnątrz naszego komponentu
useEffect(() => {
	// funkcja montowania efektu

	return () => {} // jest opcjonalną funkcją odmontowania efektu
}, [/* lista zależności */])
```

useEffect ma dwa parametry.
Pierwszym parametrem jest funkcja.
Treścią funkcji jest akcja montowania efektu.
Ta funkcja zwraca akcje odmontowania. Instrukcja "return" jest opcjonalna.

Drugi parametr to lista zależności.
Zapisujemy w niej wszystkie wartości stanów i na jakim efekcie będzie aktualizowany. 
Kiedy ta lista jest pusta, po prostu nie jest aktualizowana i montowana z komponentami.
#MR: co nie jest aktualizowane?  Czy dobrze rozuiem, że jeżeli lista jest pusta nigdy nie będzie aktualizacji?
Jeśli tablica zależności nie zostanie przekazana, efekt zostanie zaktualizowany po aktualizacji stanu każdego komponentu.

Zobaczmy jak wykorzystamy useEffect w praktyce w częstej sytuacji: pobieraniu danych.

```ts
// wewnątrz komponentu
useEffect(() => {

// Funkcja asynchroniczna do faktycznego pobierania danych i ustawiania ich w naszej aplikacji
   const fetchNote = async () => {
     const resp = await getNoteById(noteId);
     if (resp.ok) {
       const { note } = resp.data;
       joinNoteRoom(note.id);
       setCurrentNote(note.id, note.title, note.isPinned);
       setBlocks(note.blocks);
     } else {
       openToast("Failed to fetch note " + noteId, "error");
       router.push("/notes");
     }
   };

// Właściwie wywołaj tę funkcję
   fetchNote();

//Gdy komponent zostanie odmontowany lub ponownie wyrenderowany, wyczyść currentNote
   return () => {
     leaveNoteRoom(noteId);
     clearCurrentNote();
     setFocusedBlockId(null);
   };
}, [noteId]) // zależność od noteId;
```

Pokażemy wszystkie możliwości useEfeect na powyższym przykładzie.
Po pierwsze, ponieważ fetch jest opcją asynchroniczną, musimy zawinąć całą logikę pobierania w funkcję.
getNodeById() to funckcja do pobrania notatki j parametrem noteId dla indefikowania notatki.
Gdy odpowiedź na pobranie będzie dobra, po prostu ustawiamy nowe wartości.

Jeśli mówimy o zależnościach, w tym przykładzie noteId jest pobierany z adresu URL.
Zatem ten efekt będzie działał, gdy po raz pierwszy zostanie zamontowany ten komponent, a dopiero po zmianie noteId.
Po zmianie efekt noteID zostanie zaktualizowany, więc najpierw ustawiamy currentNote na null i rezygnujemy z subskrypcji na poprzedni noteId, a po załadowaniu nowej notatki ustawiamy ją i subskrybujemy na nią.
Jeśli zmieni się jakikolwiek inny stan, komponent nie będzie na to reagował.

Również ten przykład pokazuje możliwość subskrypcji w „joinNoteRoom” i „leaveNoteRoom”, które stanowią opakowanie dla pracy z socketem z biblioteki socket.io.

Lub możemy wziąć jako przykład komponent wrapper do zmiany motywu aplikacji

```ts
const MUIThemeProvider: FC<{ children: React.ReactNode }> = ({ children }) => {

  const { resolvedTheme } = useTheme();
  const [currentTheme, setCurrentTheme] = useState(lightTheme);
  ...

  useEffect(() => {
    resolvedTheme === "light"
      ? setCurrentTheme(lightTheme)
      : setCurrentTheme(darkTheme);
  }, [resolvedTheme]);
  ...

  return (
    <ThemeProvider theme={currentTheme}>
      <CssBaseline enableColorScheme />
      {children}
    </ThemeProvider>
  );
};
```

Tutaj używamy dwóch bibliotek: next-themes do kontrolowania bieżącego motywu i MUI do stylizacji naszej aplikacji. Musimy jednak zsynchronizować naszą obecną stylizację z bieżącym motywem. useState() zapisuje bieżącą konfigurację motywu i jeśli zmieniony motyw pochodzi z naszego haka, zmieniamy naszą konfigurację motywu.

W związku z tym cała aplikacja zostanie ponownie wyrenderowana, ponieważ zostanie przekazana do MuiProvider, który zastosuje style z MUI.

## Referencje (useRef()).md

Następnym i ostatnim podstawowym hookiem jest useRef().
Głównym celem jest utworzenie zmiennej, która nie będzie używana do renderowania.

```ts

//wewnątrz komponentu
const ref = useRef(0);

//zmiana i odczytanie wartości
ref.current = 15;
console.log(ref.current);

```

Powinniśmy przekazać wartość domyślną w celach informacyjnych.
Zostanie użyta tylko raz podczas montowania komponentu.
Do zapisu lub odczytu wartości używamy właściwość "current".

Nie oznacza to, że nie możemy renderować wartości referencyjnej.
Jednak jego zmiana nie spowoduje ponownego wyrenderowania komponentu.

Ten hook często ma na celu dostęp do elementów wejściowych lub przechowywanie timeoutów lub interwałów.

Timeout i interval to sposob odkładania wykonania funkcji w JavaScript.
Timeout wywoła funkcję tylko raz po pewnym czasie, gdy wywołanie interwałowe będzie działać wiele razy z pewnym odstępem między wywołaniami.

Na poniższym przykładzie pokażemy, jak można przechowywać timeout w naszej aplikacji.

```ts
...
const timeoutIdRef = useRef<NodeJS.Timeout | null>(null);

  const setCurrentNoteTitleHandler = (title: string) => {
    setCurrentNoteTitle(title);

    !editMode && setEditMode(true);
    title !== "" && (titleToSave.current = title);

    if (timeoutIdRef.current === null) {

      addChangedBlockId("title");
      timeoutIdRef.current = setTimeout(() => {
        addEvent(BATCH_EVENTS.NOTE_INFO_UPDATED_BATCH, {
          title: titleToSave.current,
          updatedAt: Date.now(),
        });

        updateNote({ id: currentNoteId!, title: titleToSave.current! });
        removeChangedBlockId("title");
        timeoutIdRef.current = null;
      }, 1000);
    }
  };
```

W tym przykładzie używam timeout, aby opóźnić wysyłanie zmian na serwer. Mówimy według typów, że ref może mieć wartość timeout lub null i ustawiamy wartość domyślną null.
#MR: mówimy według typów?  Czy może chodzi o opis typu przechowywanego w referencji?
Po dokonaniu pierwszej zmiany kafelka sprawdzamy czy limit czasu jest pusty.
Jest pusty, więc czekamy, aby funkcja wysłała zaktualizowała tytuł po pewnym czasie do bufora bieżących zmian.
Po kolejnej zmianie sprawdzamy, czy limit czasu jest pusty. Jeśli jest pusty, tworzony jest nowy limit czasu, jeśli nie, nic nie robimy.

Jest to idealne miejsce do użycia funkcji useRef(), ponieważ nie musimy wyświetlać limitu czasu.

Kolejnym typowym zastosowaniem jest sterowanie wejściami. W mojej aplikacji wystąpił problem polegający na tym, że domyślny element obszaru tekstowego nie obsługuje automatycznego rozszerzania. W tym celu napisany został niestandardowy komponent TextArea.

```ts
  

const TextArea: ForwardRefRenderFunction<HTMLTextAreaElement, TextAreaProps> = (
  { value, isFocused, style, minHeightPx = 40, ...props },
  ref
) => {  
  ...
  const innerRef = useRef<HTMLTextAreaElement>(null);
  ...
  useEffect(() => {

    if (innerRef.current) {
      innerRef.current.style.height = "inherit";
      
      innerRef.current.style.height = `${Math.max(
        innerRef.current.scrollHeight,
        minHeightPx
      )}px`;
    }
  }, [value]);

  ...
  return (
    <textarea
      ref={innerRef}
      value={value}
	  ...
    />
  );
};
```

Przekazujemy insideRef elementowi textarea do właściwości ref. 
Następnie, gdy wartość się zmienia, używamy ref, aby uzyskać właściwości elementu textarea, a mianowicie scrollHeight. 
W ten sposób dokładnie określamy, ile miejsca potrzebuje ten komponent i ustawiamy nową wysokość dla obszaru tekstowego.
 
Ostatnim przykładem jest pamięć, do której można uzyskać dostęp w efektach.
Efekt nie zawsze jest renderowany ponownie, ponieważ byłoby to zbyt kosztowne obliczeniowo.
Potrzebujemy jednak rzeczywistej wartości, więc możemy zaktualizować ref, używając innego efektu opartego na jednym stanie.
Potem możemy uzyskać dostęp do tej wartości w innym efekcie.

```ts
const BatchLayout: FC<BatchLayoutProps> = ({ noteId, children }) => {
	...
	const unsavedChanges = useRef(false);
	...
	useEffect(() => {
	    unsavedChanges.current = anyChanges;
	}, [anyChanges]);

	useEffect(() => {

    const handleBeforeUnload = (event: BeforeUnloadEvent) => {

      if (unsavedChanges.current) {
        event.preventDefault();
      }
    };
    window.addEventListener("beforeunload", handleBeforeUnload);

    return () => {
      window.removeEventListener("beforeunload", handleBeforeUnload);
    };
  }, []);

   ...
}
```

Za pomocą tego komponentu sprawdzam, czy wprowadzono jakieś zmiany, ale nie przesłano ich na serwer.
Gdy kiedy użytkownik spróbuje odświeżyć stronę na której zmiany nie przesłane jeszcze na serwer, pokazany mu zostanie dialog przeglądarki czy na pewno chce ponownie załadować stronę.

Nie musimy ponownie renderować tego efektu useEffect za każdym razem, gdy isChanges jest aktualizowany.
Więc tworzony jest tylko jeden raz przy montowaniu i aktualizujemy nasz ref, który nie zabiera dodatkowego czasu na aktualizacje efektu.

