#in_progress #to_review 
## Reducer

Kiedy już zbadaliśmy możliwości podstawowych hooków, możemy pokazać kilka hooków, które przydadzą się, gdy nasza aplikacja zacznie się rozrastać.

Istnieje wiele sytuacji, w których nasz komponent będzie miał wiele stanów i procedur obsługi zdarzeń (funkcji korzystających ze stanu).
To sprawia, że kod jest nieczytelny i trudniejszy w utrzymaniu.

Pierwszym hookiem który opiszemy jest Reducer.
Obecnie niewiele aplikacji z tego korzysta, dlatego nasza aplikacja również z tego nie korzysta.
Ale dobrze jest znać to narzędzie, aby zrozumieć dalszą logikę innych narzędzi.

Przykład, na którym pokarzemy to narzędzie jest wzięty z materiałów o Reactcie z w3school.
#MR: proponuję dodać linka do materiału, zawsze to jakiś wpis do bibliografii

```tsx
const reducer = (state, action) => {
  switch (action.type) {
    case "COMPLETE":
      return state.map((todo) => {
        if (todo.id === action.id) {
          return { ...todo, complete: !todo.complete };
        } else {
          return todo;
        }
      });
    default:
      return state;
  }
};

function Todos() {
  const [todos, dispatch] = useReducer(reducer, initialTodos);

  const handleComplete = (todo) => {
    dispatch({ type: "COMPLETE", id: todo.id });
  };

  return (
    <>
      {todos.map((todo) => (
        <div key={todo.id}>
          <label>
            <input
              type="checkbox"
              checked={todo.complete}
              onChange={() => handleComplete(todo)}
            />
            {todo.title}
          </label>
        </div>
      ))}
    </>
  );
}
```

Tworzymy funkcję zwaną reducer, która ma stan i akcje. 
Akcje to polecenia, które zostaną użyte do modyfikacji naszego stanu, tak samo jak w hooku useState, ale z niestandardowymi ustawieniami wartości.
W funkcji reducer zapisujemy wszystkie akcje i logikę pracy ze stanem dla każdej z nich.

Następnie w naszym komponencie wywołamy hook useReducer, który zaakceptuje utworzony wcześniej reducer oraz początkową wartość stanu. 
Następnie z tego hooka otrzymujemy aktualną wartość stanu i funkcje dispatch. 
Za pomocą dispatch wywołujemy akcje z naszego stanu. 
Do funckcji dispatch podajemy nazwę akcji i dodatkowe parametry dla wybranej akcji.
## Context

Reducery były użyteczne, ale zauważono, że często pewne stany i odniesienia do głęboko położonych komponentów musiały być przekazywane.
Ilość rekwizytów, które przekazywane są z komponentu do innego komponentu często zaczynała szybko rosnąć.
Często powodowało to sytuacje w których trudno było zrozumieć, które z przekazywanych rekwizytów są potrzebne dla tego komponentu, a ktore dla dziecka czy nawet głębiej.

Prostszym rozwiązaniem jest bezpośrednie przekazywanie zależności do odpowiedniego komponentu.

Popularnym wyborem było stworzenie globalnego sklepu dla stanów, w którym każdy komponent może uzyskać dostęp do części sklepu i z niego korzystać.
#MR: sklepu?  Może manedżer stanów?
Możemy także napisać w tym sklepie jakieś akcji dla konkretnych działań.
W standardowej bibliotece React nie było czegoś podobnego, dlatego w tym czasie popularność zyskały dodatkowe biblioteki, które były wykorzystywane w projektach osobistych i komercyjnych.
Najpopularniejszą biblioteką menedżerów stanów była Redux.

W React 16.3 wprowadzono context.
Przejście z poprzednich bibliotek na kontekst zajmowało dużo czasu, ale obecnie kontekst stał się popularnym menedżerem stanów.
Istnieje wiele innych menedżerów stanów i omówimy je później w części dodatkowych narzędzi React z niestandardowych bibliotek.

[Guide from FreeCodeCamp](https://www.freecodecamp.org/news/context-api-in-react/)
Aby użyć context, musimy wykonać następujące kroki:

1. Utworzyć context. W tym przykładzie naszym wspólnym elementem będzie po prostu string. Ale przeważnie w profesjonalnych projektach przechowywany jest jakiś obiekt.
```jsx
export const MyContext = createContext("");
```
2.  Zawiąć komponenty za pomocą providera (dostawcy)
```jsx
function App() {
  const [text, setText] = useState("");

  return (
    <div>
      <MyContext.Provider value={{ text, setText }}>
        <MyComponent />
      </MyContext.Provider>
    </div>
  );
}
```
3. Użyć kontekstu w komponencie głębokiego poziomu
```jsx
function MyComponent() {
  const { text, setText } = useContext(MyContext);

  return (
    <div>
      <h1>{text}</h1>
      <button onClick={() => setText('Hello, world!')}>
        Click me
      </button>
    </div>
  );
}
```

Kontekst znacznie upraszcza nasz kod.
Przejdźmy do bardziej złożonych przykładów kontekstu w naszej aplikacji.

```tsx
const BatchContext = createContext({
  events: [],
  anyEvents: false,
  addEvent: () => {},
  getAndClearEvents: () => [],
  anyChanges: false,
  setAnyChanges: () => {},
});

export const BatchProvider = ({ children }) => {
  const [events, setEvents] = useState([]);
  const [anyEvents, setAnyEvents] = useState(false);
  const [anyChanges, setAnyChanges] = useState(false);

  const addEvent = (event, data) => {
    const timeStamp = Date.now();
    setEvents((prevEvents) => [...prevEvents, { event, data, timeStamp }]);
    setAnyEvents(true);
  };

  const getAndClearEvents = () => {
    const capturedEvents = events;
    setEvents([]);
    setAnyEvents(false);
    return capturedEvents;
  };
  
  return (
    <BatchContext.Provider
      value={{
        events,
        anyEvents,
        addEvent,
        getAndClearEvents,
        anyChanges,
        setAnyChanges,
      }}
    >
      {children}
    </BatchContext.Provider>
  );
}
```
To nie jest rzeczywisty kod aplikacji, tylko ponowna implementacja menedżera stanu Zustand użytego w naszej aplikacji.

W związku z powyższym widzimy, że możemy zastąpić reducer po prostu dodając funkcje do contextu.  
Również w poprzednim przykładzie przekazujemy stan z glównego komponentu App, który faktycznie renderuje.  
Tutaj tworzymy komponent, który otacza dostawcę kontekstu i dodajemy stany do komponentu. 
Jest to dobra praktyka i jest popularnie stosowana, powoduje deklarację kontekstu i inicjowanie w jednym miejscu.

Używamy tego niestandardowego providera do pakowania całej naszej aplikacji lub tylko jej części, w której ten kontekst będzie używany.

Właściwie nasza aplikacja korzysta z kontekstu w jednym miejscu, ponieważ menedżerowie stanu utrudnią wdrożenie dalszej funkcjonalności.  
#MR: nie do końca rozumiem powyższe
Provider jest komponentem, dzięki któremu możemy zaimplementować kolejną ciekawą rzecz.

```tsx
export const ToastContext = createContext<ToastContextProps | undefined>(
  undefined
);

export const ToastProvider: React.FC<ToastProviderProps> = ({ children }) => {
  const [open, setOpen] = useState(false);
  const [message, setMessage] = useState("");
  const [severity, setSeverity] = useState<
    "success" | "info" | "warning" | "error"
  >("success");
  >
  const openToast = (
    newMessage: string,
    newSeverity: "success" | "info" | "warning" | "error" = "success"
  ) => {
    setMessage(newMessage);
    setSeverity(newSeverity);
    setOpen(true);
  };

  const closeToast = () => {
    setOpen(false);
  };
  
  const ToastComponent = (
    <Snackbar
      open={open}
      onClose={closeToast}
      ...
    >
      <Alert onClose={closeToast} severity={severity}>
        {message}
      </Alert>
    </Snackbar>
  );
  
  return (
    <ToastContext.Provider value={{ openToast }}>
      {children}
      {ToastComponent}
    </ToastContext.Provider>
  );
};
```

Nasza aplikacja musi mieć powiadomienie toastowe na wszystkich stronach. Zatem dobrą decyzją jest udostępnienie go w każdym komponencie.
W tym przykładzie tworzymy provider ToastProvider z jedną dostępną z niego funkcją - openToast(). 
Ale dla jego providera tworzymy niestandardowy ToastComponent z dodatkowymi stanami do obsługi logiki toastowania.

```tsx
export default function App({ Component, pageProps }: AppPropsWithLayout) {
  ...

  return (
    ...
        <ToastProvider>
            <Component {...pageProps} />
        </ToastProvider>
    ...
  );
}
```

Następnie korzystamy z naszego niestandardowego providera w naszym głównym komponencie aplikacji App.
A następnie używamy funkcji z kontekstu w naszym komponencie.

```tsx
function ToastExampleComponent(){
  const {openToast} = useContext(ToastContext);
  
  openToast("Toast test");
}
```


