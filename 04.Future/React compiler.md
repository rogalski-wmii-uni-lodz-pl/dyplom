#finished #to_review

[Compiler research](https://react.dev/blog/2023/03/22/react-labs-what-we-have-been-working-on-march-2023#react-optimizing-compiler)
[React blog about compiler](https://react.dev/blog/2024/02/15/react-labs-what-we-have-been-working-on-february-2024#react-compiler)

Istnieją sytuacji w Reactcie w ktorym pownownie renderowanie jest niekonieczne. #to_implenent Obecnie by pozbyc sie nadmiarowych renderowań musimy wykorzystac instrumenty "memoizing".

Jest to działające rozwiązanie, ale sprawia, że nasz kod jest trudniejszy do odczytania i napisania w ramach tej optymalizacji. Zespół React nie był z tego zadowolony od pierwszego wydania, więc w React w wersji 19 wprowadzi kompilator. Nadmiarowe renerowania będą aumotycznie pomijane przez React Compiler.  W związku z tym, memoizing zostane usuniety z React.  

Kompilacja wykona dla programistów całe zapamiętywanie, więc te hooki i funkcje nie będą istniały w przyszłości. Musimy również zauważyć, że zespół React wydaje się również wspierać projekty, w których łamane są zasady ponownego renderowania React:

> Oczywiście rozumiemy, że programiści czasami nieco naginają zasady, a naszym celem jest sprawienie, aby React Compiler działał od razu na jak największej liczbie kodu. Kompilator próbuje wykryć, kiedy kod nie jest ściśle zgodny z regułami Reacta i albo skompiluje kod w bezpiecznym miejscu, albo pominie kompilację, jeśli nie jest bezpieczna. Aby zweryfikować to podejście, testujemy w oparciu o dużą i zróżnicowaną bazę kodu Meta.
> - [React Team Blog](https://react.dev/blog/2024/02/15/react-labs-what-we-have-been-working-on-february-2024#react-compiler)

Nie jest to już funkcja eksperymentalna, została już wykorzystana w instagram.com i jest już gotowa do wdrożenia w innych usługach Meta.
